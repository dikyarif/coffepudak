<?php

return [
  'view_per_page' => 15,
  'user_avatar_path' => 'uploads/avatar/',
  'menu_image_path' => 'uploads/menu/',

  /*
  ** You can switch to s3, public
  ** if set to s3, you need to setup aws configuration in env
  */
  'storage_disk' => 'public',
  'status' => array('active', 'inactive'),
  'status_boolean' => array(1 => 'active', 0 => 'inactive'),

];