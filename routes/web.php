<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Auth */
Route::post('login', 'Auth\LoginController@postLogin');
Route::get('login','Auth\LoginController@showLoginForm')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/', 'HomeController@index');
Route::group(['middleware' => ['auth']], function() {

  Route::get('/', 'HomeController@index');

  // Users
  Route::resource('/users', 'UserController');
  // End Users

  // Profile
  Route::get('/profile', 'ProfileController@index');
  Route::post('/profile', 'ProfileController@post');
  // End Profile

  // Settings
  Route::get('/settings/menus', 'SettingController@menus');
  Route::get('/settings/addmenu', 'SettingController@addMenu');
  Route::post('/settings/addmenu', 'SettingController@postAddMenu');
  Route::get('/settings/editmenu/{id}', 'SettingController@editMenu');
  Route::post('/settings/editmenu/{id}', 'SettingController@postEditMenu');
  Route::get('/settings/deletemenu/{id}', 'SettingController@deleteMenu');

  Route::get('/settings/roles', 'SettingController@roles');
  Route::get('/settings/addrole', 'SettingController@addRole');
  Route::post('/settings/addrole', 'SettingController@postAddRole');
  Route::get('/settings/editrole/{id}', 'SettingController@editRole');
  Route::post('/settings/editrole/{id}', 'SettingController@postEditRole');
  Route::get('/settings/editrolepermission/{id}', 'SettingController@editRolePermission');
  Route::post('/settings/editrolepermission/{id}', 'SettingController@postEditRolePermission');


  Route::get('/settings/permissions', 'SettingController@permissions');
  Route::get('/settings/addpermission', 'SettingController@addPermission');
  Route::post('/settings/addpermission', 'SettingController@postAddPermission');
  Route::get('/settings/editpermission/{id}', 'SettingController@editPermission');
  Route::post('/settings/editpermission/{id}', 'SettingController@postEditPermission');

  // End settings

  Route::get('/daftar-menu', 'MenuController@index');
  Route::get('/daftar-menu/create', 'MenuController@create');
  Route::post('/daftar-menu/store', 'MenuController@store');
  Route::get('/daftar-menu/{id}/edit', 'MenuController@edit');
  Route::post('/daftar-menu/{id}/update', 'MenuController@update');

  Route::get('/cashier', 'CashierController@index');

  Route::get('/keranjang/gantimeja', 'KeranjangController@gantimeja');
  Route::get('/keranjang/pilih-meja', 'KeranjangController@pilihmeja');
  Route::get('/keranjang/store', 'KeranjangController@store');
  Route::get('/keranjang/{id}/delete', 'KeranjangController@delete');
  Route::get('/keranjang/decrease', 'KeranjangController@decrease');
  Route::get('/keranjang/showpesanan', 'KeranjangController@showpesanan');
  Route::post('/keranjang/submitpesanan', 'KeranjangController@storePesanan');
  Route::post('/keranjang/pembayaran/{meja_id}', 'KeranjangController@order');

  Route::get('/transaksi', 'TransaksiController@index');

  Route::get('/laporan', 'LaporanController@index');

  Route::get('/meja', 'MejaController@index');

  Route::get('/meja/create', 'MejaController@createform');

  Route::get('/meja/{id}/delete', 'MejaController@delete');

  Route::post('/meja/create', 'MejaController@create');

  Route::get('/meja/{id}/edit', 'MejaController@editform');

  Route::post('/meja/{id}/edit', 'MejaController@update');

  Route::get('/users/{id}/delete', 'UserController@delete');
});

