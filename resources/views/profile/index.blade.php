@extends('adminlte::page')

@section('title', $meta_title)

@section('content_header')
    <h1>{{$meta_title}}</h1>
@stop

@section('content')

  @if (session('message'))
    <div class="alert alert-info alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{session('message')}}
    </div>
  @endif

  <div class="box">

    <form method="POST" class="form-horizontal" action="{{ url('profile') }}" enctype="multipart/form-data">
      {{csrf_field()}}
      <div class="box-body">

        <div class="form-group has-feedback{{ $errors->has('name') ? ' has-error' : '' }}">
          <label for="name" class="col-sm-2 control-label">Name *</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" value="{{ $data->name }}" id="name" name="name" placeholder="Name" required autofocus>
            @if ($errors->has('name'))
                <span class="help-block error">
                    <p>{{ $errors->first('name') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->

        <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
          <label for="email" class="col-sm-2 control-label">Email *</label>
          <div class="col-sm-10">
            <input type="email" class="form-control" value="{{ $data->email }}" disabled id="email" name="email" placeholder="Email" autofocus>
            @if ($errors->has('email'))
                <span class="help-block error">
                    <p>{{ $errors->first('email') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->

        <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
          <label for="password" class="col-sm-2 control-label">Password</label>
          <div class="col-sm-10">
            <input type="password" class="form-control" value="" id="password" name="password" placeholder="Password">
            <span class="help-block error">
              <p>Leave it blank, if You don't want to change password</p>
            </span>
            @if ($errors->has('password'))
                <span class="help-block error">
                    <p>{{ $errors->first('password') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->

        <div class="form-group has-feedback{{ $errors->has('avatar') ? ' has-error' : '' }}">
          <label for="avatar" class="col-sm-2 control-label">Avatar</label>
          <div class="col-sm-10">
            @if($data->avatar)
              <img src="{{ Storage::disk(Config('constant.storage_disk'))->url(Config('constant.user_avatar_path').$data->avatar) }}" width="100" style="margin-bottom:15px;">
            @endif
            <input type="file" name="avatar" id="image" class="form-control">
            <span class="help-block error">
              <p>Leave it blank, if you don't want to upload avatar. Size max 1Mb must be square eg(200px x 200px)</p>
            </span>
            @if ($errors->has('avatar'))
                <span class="help-block error">
                    <p>{{ $errors->first('avatar') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->

      </div><!-- end box-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Save</button>
        <a href="{{url('users')}}" class="btn btn-warning">Back</a>
      </div><!-- end box-footer -->
    </form>

  </div><!-- end box -->

@stop