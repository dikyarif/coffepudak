@extends('adminlte::page')

@section('title', $meta_title)

@section('content_header')
    <h1>{{$meta_title}}</h1>
@stop

@section('content')

  @if (session('message'))
    <div class="alert alert-info alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{session('message')}}
    </div>
  @endif

  <div class="box">

    <form method="POST" class="form-horizontal" action="{{ url('daftar-menu/store') }}" enctype="multipart/form-data">
      {{csrf_field()}}

      <div class="box-body">

        <div class="form-group has-feedback{{ $errors->has('name') ? ' has-error' : '' }}">
          <label for="name" class="col-sm-2 control-label">Nama Menu *</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" value="" id="name" name="name" placeholder="Nama Menu" required autofocus>
            @if ($errors->has('name'))
                <span class="help-block error">
                    <p>{{ $errors->first('name') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->

        <div class="form-group has-feedback{{ $errors->has('jenis') ? ' has-error' : '' }}">
          <label for="email" class="col-sm-2 control-label">Jenis *</label>
          <div class="col-sm-10">
            <select id="" class="form-control" name="jenis" required>
              <option value="makanan">Makanan</option>
              <option value="minuman">Minuman</option>
              <option value="others">Others</option>
            </select>
            @if ($errors->has('jenis'))
                <span class="help-block error">
                    <p>{{ $errors->first('jenis') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->
       
        <div class="form-group has-feedback{{ $errors->has('modal') ? ' has-error' : '' }}">
          <label for="email" class="col-sm-2 control-label">Modal *</label>
          <div class="col-sm-10">
            <input type="number" name="modal" class="form-control" required>
            @if ($errors->has('modal'))
                <span class="help-block error">
                    <p>{{ $errors->first('modal') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->

        <div class="form-group has-feedback{{ $errors->has('harga') ? ' has-error' : '' }}">
          <label for="email" class="col-sm-2 control-label">Harga Jual *</label>
          <div class="col-sm-10">
            <input type="number" name="harga" class="form-control" required>
            @if ($errors->has('harga'))
                <span class="help-block error">
                    <p>{{ $errors->first('harga') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->
        
        <div class="form-group has-feedback{{ $errors->has('status') ? ' has-error' : '' }}">
          <label for="email" class="col-sm-2 control-label">Status *</label>
          <div class="col-sm-10">
            <select name="status" id="" class="form-control" name="Status">
              <option value="0">Inactive</option>
              <option value="1">Active</option>
            </select>
            @if ($errors->has('status'))
                <span class="help-block error">
                    <p>{{ $errors->first('status') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->
        
        <div class="form-group has-feedback{{ $errors->has('image') ? ' has-error' : '' }}">
          <label for="avatar" class="col-sm-2 control-label">Image</label>
          <div class="col-sm-10">
            <input type="file" name="image" id="image" class="form-control">
            <span class="help-block error">
              <p>Leave it blank, if you don't want to upload image. Size max 1Mb must be square eg(200px x 200px)</p>
            </span>
            @if ($errors->has('image'))
                <span class="help-block error">
                    <p>{{ $errors->first('image') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->

      </div><!-- end box-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Save</button>
        <a href="{{url('daftar-menu')}}" class="btn btn-warning">Back</a>
      </div><!-- end box-footer -->
    </form>

  </div><!-- end box -->

@stop