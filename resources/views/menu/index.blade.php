@extends('adminlte::page')

@section('title', $meta_title)

@section('content_header')
    <h1>{{$meta_title}}</h1>
@stop

@section('content')

  @if (session('message'))
    <div class="alert alert-info alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{session('message')}}
    </div>
  @endif

  <div class="row">

    <div class="col-md-12">

      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">{{$meta_title}} List</h3>


        </div><!-- end box-header -->

        <div class="box-body table-responsive">
          <div class="pull-right">
            <a href="{{ url('daftar-menu/create') }}" class="btn btn btn-info">Add New</a>
          </div>

          <table class="table table-hover">
            <thead>
              <tr>
                <th>ID</th>
                <th>Image</th>
                <th>Nama</th>
                <th>Modal</th>
                <th>Harga Jual</th>
                <th>Jenis</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              @if(count($datas) > 0)
                @foreach($datas as $data)
                  <tr>
                    <td>{{ $data->id }}</td>
                    <td>
                      @if($data->image)
                        <img src="{{ Storage::disk(Config('constant.storage_disk'))->url(Config('constant.menu_image_path').$data->image) }}" width="45" class="img-square" alt="{{ $data->name }}">
                      @else
                        <img src="{{ asset('images/avatar.jpg') }}" width="45" class="img-circle" alt="{{ $data->name }}">
                      @endif

                    </td>
                    <td>{{ $data->name }}</td>
                    <td>Rp {{ number_format($data->modal,2,',','.') }}</td>
                    <td>Rp {{ number_format($data->harga,2,',','.') }}</td>
                    <td>{{ $data->jenis }}</td>
                    <td>{{ $data->status == 1 ? 'Active' : 'Inactive' }}</td>
                    <td>
                      <a href="{{url('daftar-menu/'.$data->id.'/edit')}}" class="btn btn-primary" title="Edit"><i class="fa fa-pencil"></i></a>
                    </td>
                  </tr>
                @endforeach
              @else
                <tr><td colspan="7">No result found.</td></tr>
              @endif
            </tbody>
           
          </table>
        
        </div><!-- end box-body -->

        <div class="box-footer clearfix">
          @if(count($datas) > 0)
            <div class="pull-left">
              <p><strong>Showing {{ $datas->firstItem() }} to {{ $datas->lastItem() }} of {{ $datas->total() }} entries</strong></p>
            </div>
            <div class="pull-right">
              {{ $datas->links() }}
            </div>
          @endif
        </div><!-- end box-footer -->
        

      </div><!-- end box -->

    </div><!-- end col-md-12 -->

  </div><!-- end row -->

@stop