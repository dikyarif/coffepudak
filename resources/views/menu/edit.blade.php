@extends('adminlte::page')

@section('title', $meta_title)

@section('content_header')
    <h1>{{$meta_title}}</h1>
@stop

@section('content')

  @if (session('message'))
    <div class="alert alert-info alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{session('message')}}
    </div>
  @endif

  <div class="box">

    <form method="POST" class="form-horizontal" action="{{ url('daftar-menu/'.$data->id.'/update') }}" enctype="multipart/form-data">
      {{csrf_field()}}
      <div class="box-body">

        <div class="form-group has-feedback{{ $errors->has('name') ? ' has-error' : '' }}">
          <label for="name" class="col-sm-2 control-label">Name *</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" value="{{ $data->name }}" id="name" name="name" placeholder="Name" required autofocus>
            @if ($errors->has('name'))
                <span class="help-block error">
                    <p>{{ $errors->first('name') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->
        
        <div class="form-group has-feedback{{ $errors->has('jenis') ? ' has-error' : '' }}">
          <label for="email" class="col-sm-2 control-label">Jenis *</label>
          <div class="col-sm-10">
            <select id="" class="form-control" name="jenis" required>
              <option value="makanan" {{ $data->jenis == 'makanan' ? 'selected' : '' }}>Makanan</option>
              <option value="minuman" {{ $data->jenis == 'minuman' ? 'selected' : '' }}>Minuman</option>
              <option value="others" {{ $data->jenis == 'others' ? 'selected' : '' }}>Others</option>
            </select>
            @if ($errors->has('jenis'))
                <span class="help-block error">
                    <p>{{ $errors->first('jenis') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->
        
         <div class="form-group has-feedback{{ $errors->has('modal') ? ' has-error' : '' }}">
          <label for="name" class="col-sm-2 control-label">Modal *</label>
          <div class="col-sm-10">
            <input type="number" class="form-control" value="{{ $data->modal }}" id="modal" name="modal" placeholder="Modal" required autofocus>
            @if ($errors->has('modal'))
                <span class="help-block error">
                    <p>{{ $errors->first('modal') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->

        <div class="form-group has-feedback{{ $errors->has('harga') ? ' has-error' : '' }}">
          <label for="name" class="col-sm-2 control-label">Harga Jual *</label>
          <div class="col-sm-10">
            <input type="number" class="form-control" value="{{ $data->harga }}" id="harga" name="harga" placeholder="Harga" required autofocus>
            @if ($errors->has('harga'))
                <span class="help-block error">
                    <p>{{ $errors->first('harga') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->

        <div class="form-group has-feedback{{ $errors->has('status') ? ' has-error' : '' }}">
          <label for="status" class="col-sm-2 control-label">Status</label>
          <div class="col-sm-10">
            <select name="status" class="form-control">
              @php
                $statusess = Config('constant.status_boolean');
              @endphp

              @foreach($statusess as $key => $name)
                <option value="{{$key}}" {{$name == $data->status ? 'selected' : ''}}>{{ucfirst($name)}}</option>
              @endforeach
            </select>
            @if ($errors->has('status'))
                <span class="help-block error">
                    <p>{{ $errors->first('status') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->

        <div class="form-group has-feedback{{ $errors->has('avatar') ? ' has-error' : '' }}">
          <label for="avatar" class="col-sm-2 control-label">Avatar</label>
          <div class="col-sm-10">
            @if($data->avatar)
              <img src="{{ Storage::disk(Config('constant.storage_disk'))->url(Config('constant.user_avatar_path').$data->avatar) }}" width="100" style="margin-bottom:15px;">
            @endif
            <input type="file" name="avatar" id="image" class="form-control">
            <span class="help-block error">
              <p>Leave it blank, if you don't want to upload avatar. Size max 1Mb must be square eg(200px x 200px)</p>
            </span>
            @if ($errors->has('avatar'))
                <span class="help-block error">
                    <p>{{ $errors->first('avatar') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->

      </div><!-- end box-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Save</button>
        <a href="{{url('users')}}" class="btn btn-warning">Back</a>
      </div><!-- end box-footer -->
    </form>

  </div><!-- end box -->

@stop