@extends('adminlte::page')

@section('title', $meta_title)

@section('content_header')
    <h1>{{$meta_title}}</h1>
@stop

@section('content')

  @if (session('message'))
    <div class="alert alert-info alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{session('message')}}
    </div>
  @endif

  <div class="box">

    <form method="POST" class="form-horizontal" action="{{ url('settings/editrolepermission/'.$id) }}">
      {{csrf_field()}}

      <div class="box-body">

        <div class="form-group has-feedback{{ $errors->has('name') ? ' has-error' : '' }}">
          <label for="name" class="col-sm-2 control-label">Permission</label>
          <div class="col-sm-10">
            <div class="scroll-box">
              @foreach($permissions as $permission)
                <div class="checkbox-container">
                  @php
                    $selected = "";
                    if(in_array($permission->id, $selected_permissions)){
                      $selected = "checked";
                    }
                  @endphp
                  <input class="checkbox-menu" type="checkbox" name="permissions[]" value="{{$permission->id}}" id="permission-{{$permission->id}}" {{$selected}}><label for="permission-{{$permission->id}}">{{$permission->display_name}}</label>
                </div>
              @endforeach
            </div><!-- end scroll-box -->
            <a href="javascript:" title="Check All" class="checkall">Check All</a> | <a href="javascript:" title="Uncheck All" class="uncheckall">Uncheck All</a><br><br>
          </div>
        </div><!-- end form-group -->

      </div><!-- end box-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Save</button>
        <a href="{{url('settings/roles')}}" class="btn btn-warning">Back</a>
      </div><!-- end box-footer -->
    </form>

  </div><!-- end box -->

@stop

@section('js')
  <script type="text/javascript">
      $('.checkall').click(function() {
          $('.checkbox-menu').prop("checked", true);
      });

      $('.uncheckall').click(function() {
          $('.checkbox-menu').prop("checked", false);
      });
  </script>
@stop