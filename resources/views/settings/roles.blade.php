@extends('adminlte::page')

@section('title', $meta_title)

@section('content_header')
    <h1>{{$meta_title}}</h1>
@stop

@section('content')

  @if (session('message'))
    <div class="alert alert-info alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{session('message')}}
    </div>
  @endif

  <div class="row">

    <div class="col-md-12">

      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">{{$meta_title}} List</h3>
        </div><!-- end box-header -->

        <div class="box-body table-responsive">
          <div class="pull-right">
            <a href="{{ url('settings/addrole') }}" class="btn btn btn-info">Add New</a>
          </div>
          <table class="table table-hover">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Display Name</th>
                <th>Description</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              @if(count($datas) > 0)
                @foreach($datas as $data)
                  <tr>
                    <td>{{ $data->id }}</td>
                    <td>{{ $data->name }}</td>
                    <td>{{ $data->display_name }}</td>
                    <td>{{ $data->description }}</td>
                    <td>
                      <a href="{{url('settings/editrole/'.$data->id)}}" class="btn btn-primary" title="Edit"><i class="fa fa-pencil"></i></a>
                      @if(auth()->user()->hasRole('super_administrator'))
                        <a href="{{url('settings/editrolepermission/'.$data->id)}}" class="btn btn-info" title="Set Permission"><i class="fa fa-unlock"></i></a>
                      @endif
                    </td>
                  </tr>
                @endforeach
              @else
                <tr><td colspan="5">No result found.</td></tr>
              @endif
            </tbody>
          </table>

        </div><!-- end box-body -->

        <div class="box-footer clearfix">
          @if(count($datas) > 0)
            <div class="pull-left">
    					<p><strong>Showing {{ $datas->firstItem() }} to {{ $datas->lastItem() }} of {{ $datas->total() }} entries</strong></p>
    				</div>
    				<div class="pull-right">
    					{{$datas->links()}}
    				</div>
          @endif
        </div><!-- end box-footer -->

      </div><!-- end box -->

    </div><!-- end col-md-12 -->

  </div><!-- end row -->

@stop