@extends('adminlte::page')

@section('title', $meta_title)

@section('content_header')
    <h1>{{$meta_title}}</h1>
@stop

@section('content')

  @if (session('message'))
    <div class="alert alert-info alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{session('message')}}
    </div>
  @endif

  <div class="box">

    <form method="POST" class="form-horizontal" action="{{ url('settings/editrole/'.$data->id) }}">
      {{csrf_field()}}

      <div class="box-body">

        <div class="form-group has-feedback{{ $errors->has('name') ? ' has-error' : '' }}">
          <label for="name" class="col-sm-2 control-label">Name *</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" disabled value="{{ $data->name }}" id="name" name="name" placeholder="Name" autofocus>
            @if ($errors->has('name'))
                <span class="help-block error">
                    <p>{{ $errors->first('name') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->

        <div class="form-group has-feedback{{ $errors->has('display_name') ? ' has-error' : '' }}">
          <label for="display_name" class="col-sm-2 control-label">Display Name *</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" value="{{ $data->display_name }}" id="display_name" name="display_name" placeholder="Display Name" required autofocus>
            @if ($errors->has('display_name'))
                <span class="help-block error">
                    <p>{{ $errors->first('url') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->

        <div class="form-group has-feedback{{ $errors->has('description') ? ' has-error' : '' }}">
          <label for="description" class="col-sm-2 control-label">Description</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" value="{{ $data->description }}" id="description" name="description" placeholder="Description">
            @if ($errors->has('description'))
                <span class="help-block error">
                    <p>{{ $errors->first('description') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->

      </div><!-- end box-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Save</button>
        <a href="{{url('settings/roles')}}" class="btn btn-warning">Back</a>
      </div><!-- end box-footer -->
    </form>

  </div><!-- end box -->

@stop