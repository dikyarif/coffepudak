@extends('adminlte::page')

@section('title', $meta_title)

@section('content_header')
    <h1>{{$meta_title}}</h1>
@stop

@section('content')

  @if (session('message'))
    <div class="alert alert-info alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{session('message')}}
    </div>
  @endif

  <div class="row">

    <div class="col-md-12">

      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">{{$meta_title}} List</h3>
        </div><!-- end box-header -->

        <div class="box-body table-responsive">
          <div class="pull-right">
            <a href="{{ url('settings/addmenu') }}" class="btn btn btn-info">Add New</a>
          </div>

          <table class="table table-hover">
            <thead>
              <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Parent</th>
                <th>URL</th>
                <th>Icon</th>
                <th>Status</th>
                <th>Visibility</th>
                <th>Permission</th>
                <th>Sort Order</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              @if(count($datas) > 0)
                @foreach($datas as $data)
                  <tr>
                    <td>{{ $data->id }}</td>
                    <td>{{ $data->title }}</td>
                    <td>{{ $data->parent }}</td>
                    <td>{{ $data->url }}</td>
                    <td>{{ $data->icon }}</td>
                    <td>{{ $data->status }}</td>
                    <td>{{ $data->visibility }}</td>
                    <td>{{ $data->permission }}</td>
                    <td>{{ $data->sort_order }}</td>
                    <td>
                      <a href="{{url('settings/editmenu/'.$data->id)}}" class="btn btn-primary" title="Edit"><i class="fa fa-pencil"></i></a>

                      @if(auth()->user()->hasRole('super_administrator'))
                        <a href="{{url('settings/deletemenu/'.$data->id)}}" onclick="return confirm('Are You sure to delete this item?')" class="btn btn-danger" title="Delete"><i class="fa fa-trash"></i></a>
                      @endif
                    </td>
                  </tr>
                @endforeach
              @else
                <tr><td colspan="10">No result found.</td></tr>
              @endif
            </tbody>
          </table>

        </div><!-- end box-body -->

        <div class="box-footer clearfix">
          @if(count($datas) > 0)
            <div class="pull-left">
    					<p><strong>Showing {{ $datas->firstItem() }} to {{ $datas->lastItem() }} of {{ $datas->total() }} entries</strong></p>
    				</div>
    				<div class="pull-right">
    					{{$datas->links()}}
    				</div>
          @endif
        </div><!-- end box-footer -->

      </div><!-- end box -->

    </div><!-- end col-md-12 -->

  </div><!-- end row -->

@stop