@extends('adminlte::page')

@section('title', $meta_title)

@section('content_header')
    <h1>{{$meta_title}}</h1>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/chosen.min.css') }}">
@stop

@section('content')

  @if (session('message'))
    <div class="alert alert-info alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{session('message')}}
    </div>
  @endif

  <div class="box">

    <form method="POST" class="form-horizontal" action="{{ url('settings/addmenu/') }}">
      {{csrf_field()}}

      <div class="box-body">

        <div class="form-group has-feedback{{ $errors->has('parent') ? ' has-error' : '' }}">
          <label for="parent" class="col-sm-2 control-label">Parent *</label>
          <div class="col-sm-10">
            <select name="parent" class="form-control" required>
              <option value="0"{{ (old('parent') == 0) ? ' selected' : '' }}>No Parent</option>
              @if(count($parents) > 0)
                @foreach($parents as $parent)
                  <option value="{{ $parent->id }}"{{ (old('parent') == $parent->id) ? ' selected' : '' }}>{{ $parent->title }}</option>
                @endforeach
              @endif
            </select>
            @if ($errors->has('parent'))
                <span class="help-block error">
                    <p>{{ $errors->first('parent') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->

        <div class="form-group has-feedback{{ $errors->has('title') ? ' has-error' : '' }}">
          <label for="title" class="col-sm-2 control-label">Title *</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" value="{{ old('title') }}" id="title" name="title" placeholder="Title" required autofocus>
            @if ($errors->has('title'))
                <span class="help-block error">
                    <p>{{ $errors->first('title') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->

        <div class="form-group has-feedback{{ $errors->has('url') ? ' has-error' : '' }}">
          <label for="url" class="col-sm-2 control-label">URL *</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" value="{{ old('url') }}" id="url" name="url" placeholder="URL" required autofocus>
            <span class="help-block">
              <p>If there is no URL, fill it with "#"</p>
            </span>
            @if ($errors->has('url'))
                <span class="help-block error">
                    <p>{{ $errors->first('url') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->

        <div class="form-group has-feedback{{ $errors->has('icon') ? ' has-error' : '' }}">
          <label for="icon" class="col-sm-2 control-label">Icon</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" value="{{ old('icon') }}" id="icon" name="icon" placeholder="Icon">
            <span class="help-block">
              <p>Using fontawesome icon</p>
            </span>
            @if ($errors->has('icon'))
                <span class="help-block error">
                    <p>{{ $errors->first('icon') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->

        <div class="form-group has-feedback{{ $errors->has('status') ? ' has-error' : '' }}">
          <label for="status" class="col-sm-2 control-label">Status</label>
          <div class="col-sm-10">
            <select name="status" class="form-control">
              <option value="0"{{ (old('status') == 0) ? ' selected' : '' }}>Inactive</option>
              <option value="1"{{ (old('status') == 1) ? ' selected' : '' }}>Active</option>
            </select>
            @if ($errors->has('status'))
                <span class="help-block error">
                    <p>{{ $errors->first('status') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->

        <div class="form-group has-feedback{{ $errors->has('visibility') ? ' has-error' : '' }}">
          <label for="visibility" class="col-sm-2 control-label">Visibility</label>
          <div class="col-sm-10">
            <select name="visibility" class="form-control">
              <option value="0"{{ (old('visibility') == 0) ? ' selected' : '' }}>Inactive</option>
              <option value="1"{{ (old('visibility') == 1) ? ' selected' : '' }}>Active</option>
            </select>
            @if ($errors->has('visibility'))
                <span class="help-block error">
                    <p>{{ $errors->first('visibility') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->

        <div class="form-group has-feedback{{ $errors->has('permission') ? ' has-error' : '' }}">
          <label for="permission" class="col-sm-2 control-label">Permission</label>
          <div class="col-sm-10">
            <select id="permission" name="permission" class="form-control">
              <option value="">Choose One</option>
              @if(count($permissions) > 0)
                @foreach($permissions as $permission)
                  <option value="{{ $permission->name }}"{{ (old('permission') == $permission->name) ? ' selected' : '' }}>{{ $permission->display_name }}</option>
                @endforeach
              @endif
            </select>
            @if ($errors->has('permission'))
                <span class="help-block error">
                    <p>{{ $errors->first('permission') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->

        <div class="form-group has-feedback{{ $errors->has('sort_order') ? ' has-error' : '' }}">
          <label for="sort_order" class="col-sm-2 control-label">Sort Order</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" value="{{ old('sort_order') ? $data->sort_order : 0 }}" id="sort_order" name="sort_order" placeholder="URL">

            @if ($errors->has('sort_order'))
                <span class="help-block error">
                    <p>{{ $errors->first('sort_order') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->

      </div><!-- end box-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Save</button>
        <a href="{{url('settings/menus')}}" class="btn btn-warning">Back</a>
      </div><!-- end box-footer -->
    </form>

  </div><!-- end box -->

@stop

@section('js')
    <script type="text/javascript" src="{{asset('js/chosen.jquery.min.js')}}"></script>
    <script>
      $('#permission').chosen({
        no_results_text: "Oops, No result found!"
      });
    </script>
@stop