<!DOCTYPE html>
<html>
<head>
	<title>{{$meta_title}}</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="/css/new_style.css" type="text/css" />
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Raleway+Dots" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.min.js"></script>
</head>
<body>
	<div class="wrapper-btn-payment">
		<button class="btn-payment" data-toggle="modal" data-target="#modalMeja">
			<i class="fa fa-money"></i> <br>
			PEMBAYARAN
		</button>
	</div>
	<div class="modal fade" id="modalMeja" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Choose Table</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">

	        @foreach($table_pesanan as $pesan)
				<button class="btn btn-primary meja-pesanan" style="text-align: left;" onclick="modalPembayaran({{$pesan->meja_id}})" data-id="{{$pesan->meja_id}}" >{{$pesan->mejas->name}} <br> <small>{{$pesan->created_at}}</small> </button> <small><a href="{{ url( '/keranjang/'.$pesan->meja_id.'/delete' ) }}"><i class="fa fa-trash"></i></a> <button onclick="pilihMeja({{$pesan->meja_id}});$('.meja-pesanan').addClass('clicked')" class="btn btn-sm btn-danger">Ganti Meja</button></small><br><br>
				
	        @endforeach
	        <div class="pilih-meja">
					
				</div>
	      </div>
	      
	    </div>
	  </div>
	</div>
	
	<div class="modal fade" id="modalPembayaran" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Pembayaran</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body" id="body-pembayaran">
	      	
	      </div>
	      
	    </div>
	  </div>
	</div>


	<div class="container-fluid">
	 	@if (session('message'))
		    <script>swal('{{session("message")}}');</script>
		@endif
		<div class="row">
			<div class="col-md-4 big-bg-left">
				<div class="pesanan">
					<center>
						<div class="head">
							Ordered Item
						</div>
						
					</center>

					
					<div id="order-item">
					{{count($keranjangs) == 0 ? 'Empty Order' : ''}}
					@php
						
						$total = 0; $sub_total = 0;

					@endphp
						@if(count($keranjangs) > 0)
							@foreach($keranjangs as $key => $keranjang)
								<div class="item">{{$key+1}}. {{$keranjang->menu->name}}<br/><small> jumlah: <div class="pull-right">{{$keranjang->jumlah}}</div><br/>harga satuan: <div class="pull-right">Rp{{$keranjang->menu->harga}}</div></small></div>
								@php
									$sub_total += ($keranjang->menu->harga*$keranjang->jumlah);
								@endphp
							@endforeach
							@php
								$total = $sub_total + ($sub_total/10);
							@endphp
							<div class="item">Sub Total<div class="pull-right"><small>Rp</small>{{$sub_total}}</div></div>

							<div class="item"><small>Ppn<div class="pull-right">10%</div></small></div>

							<div class="item"><b>Total</b><div class="pull-right"><small>Rp</small><b>{{$total}}</b></div></div>

							<form action="{{ url('keranjang/submitpesanan')}}" method="POST" class="form-inline">
							{{ csrf_field() }}<center><button class="btn btn-default" type="submit">Submit</button></center>
								
							</form>

						@endif
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="row">
					<nav class="navbar navbar-inverse dark">
					  <div class="container-fluid">
					    <div class="navbar-header">
					      <a class="navbar-brand" href="#">
					        <b>Coffe</b>Pudak
					      </a>
					    </div>
					    <ul class="nav navbar-nav navbar-right">
					    	<li>
					    		<a href="#"><i class="fa fa-user"></i> {{auth()->user()->name}}</a>
					    	</li>
					    	<li>
					    		<a href="/"><i class="fa fa-minus-square"></i>  Dashboard</a>
					    	</li>
					    </ul>
					  </div>
					</nav>	
				</div>
				<div class="row row-pilih-meja">
					<div class="col-md-12 heading">
							Choose Table
					</div>
					<div class="row mejas">
						@foreach($tables as $meja)
						<div class="col-md-2 ">
							<button class="meja @foreach($pesanan as $pesan) @if($pesan->meja_id == $meja->id) @php echo 'disabled'; break; @endphp @endif @endforeach normal" id='meja-{{$meja->id}}' data-id='{{$meja->id}}' onclick="toggleMeja({{$meja->id}})" @foreach($pesanan as $pesan) @if($pesan->meja_id == $meja->id) @php echo 'disabled'; break; @endphp @endif @endforeach>
								<center><i class="fa fa-coffee"></i> <br> {{$meja->name}}</center>
							</button>
						</div>

						@endforeach	
					</div>
				</div>
				<div id="wrapper-menu">
					<div class="row">
						<div class="col-md-12 bg-coffee">
							<center>
								<h2>Drinks</h2>
							</center>
						</div>
					</div>
					<div class="row menu">
						@if(count($menus) > 0)
											
							@foreach($menus as $menu)
								@if($menu->jenis == 'minuman')
									<div class="col-md-12 no-padd">
										<div class="menu-item form-control btn-default">
											{{$menu->name}} <div class="pull-right"><button class="btn menu-item-decrease" data-id="{{$menu->id}}" style="margin-right: 5px;"><i class="fa fa-minus-square"></i></button> <button data-id="{{$menu->id}}" class="btn menu-item-add"><i class="fa fa-plus-square"></i></button></div>
										</div>
									</div>
								@endif
							@endforeach
											
						@endif
					</div>
					<div class="row">
						<div class="col-md-12 bg-food">
							<center>
								<h2>Food</h2>
							</center>
						</div>
					</div>
					<div class="row menu">
						@if(count($menus) > 0)
														
							@foreach($menus as $menu)
								@if($menu->jenis == 'makanan')
									<div class="col-md-12 no-padd">
										<div class="menu-item form-control btn-default">
											{{$menu->name}} <div class="pull-right"><button class="btn menu-item-decrease" data-id="{{$menu->id}}" style="margin-right: 5px;"><i class="fa fa-minus-square"></i></button> <button data-id="{{$menu->id}}" class="btn menu-item-add"><i class="fa fa-plus-square"></i></button></div>
										</div>
									</div>
								@endif
							@endforeach
											
						@endif
					</div>
				</div>
					
			</div>
		</div>
	</div>

</body>

<script>
	function pilihMeja(idMeja){
		var table = idMeja ;

		var source = 'keranjang/pilih-meja?table_id=' +  table;

		var opt = '';

		$.get(source, function(data) {

			if (data.length > 0) {

				$.each(data, function(i, el)  {
					var table = $('.meja-pesanan.clicked').attr('data-id');
					opt += '<a href="/keranjang/gantimeja?table=' + table +'&new=' + el.id + '" class="btn btn-sm btn-success">' + el.name +'</a> ';

				});

					$('.pilih-meja').html(opt);
			}
			else
			{
				opt += 'tidak ada meja kosong';
				$('.pilih-meja').html(opt);
			}
		});
	}
	function toggleMeja(id){
		$('#wrapper-menu').css('display','block');
		$('.meja').not(this).removeClass('clicked');
		$('#meja-'+id).toggleClass('normal clicked');
	}
	function modalPembayaran(id){

		$('#modalMeja').modal('hide');

		var table = id ;

		var source = 'keranjang/showpesanan?table_id=' +  table;

		$.get(source, function(data) {

			if (data.length > 0) {

				var opt = '';

				var sub_total = 0;

				var total = 0;

				var meja_id = '';

				$.each(data, function(i, el)  {

					opt += '<div class="item">'+ (i+1) +'. '+el.name+ '<br/><small> jumlah: <div class="pull-right">'+ el.jumlah + '</div><br/>harga satuan: <div class="pull-right">Rp' + el.harga + '</div></small></div>';

					sub_total += (el.harga*el.jumlah);

					meja_id = el.meja_id;

				});

					total = sub_total + (sub_total / 10);

					opt += '<div class="item">Sub Total<div class="pull-right"><small>Rp</small>' + sub_total + '</div></div>';

					opt += '<div class="item"><small>Ppn<div class="pull-right">10%</div></small></div>';

					opt += '<div class="item"><b>Total</b><div class="pull-right"><small>Rp</small><b>' + total + '</b></div></div>';

					opt += '<br><form action="keranjang/pembayaran/'+ meja_id +'" method="POST" class="form-inline"> {{ csrf_field() }}<label>Masukkan jumlah uang </label> <input type="number" name="uang" required style="color: black" min="' + total + '"> <button class="btn btn-default" type="submit">Submit</button></form>';

					console.log(source);

					$('#body-pembayaran').html(opt);

					$('#modalPembayaran').modal('toggle');

			}
			else
			{
				console.log('No data Found');
			}
		});
	}
	$(document).ready(function(){

		$('.menu-item-add').click(function(){

			var param = $(this).attr('data-id');

			var table = $('.meja.clicked').attr('data-id');

			var source = 'keranjang/store?id=' + param + '&table=' + table;

			$.get(source, function(data) {

				if (data.length > 0) {

					var opt = '';

					var sub_total = 0;

					var total = 0;

					$.each(data, function(i, el)  {

						opt += '<div class="item">'+ (i+1) +'. '+el.name+ '<br/><small> jumlah: <div class="pull-right">'+ el.jumlah + '</div><br/>harga satuan: <div class="pull-right">Rp' + el.harga + '</div></small></div>';

					sub_total += (el.harga*el.jumlah);

					});

					total = sub_total + (sub_total / 10);

					opt += '<div class="item">Sub Total<div class="pull-right"><small>Rp</small>' + sub_total + '</div></div>';

					opt += '<div class="item"><small>Ppn<div class="pull-right">10%</div></small></div>';

					opt += '<div class="item"><b>Total</b><div class="pull-right"><small>Rp</small><b>' + total + '</b></div></div>';

					opt += '<form action="keranjang/submitpesanan" method="POST" class="form-inline"> {{ csrf_field() }}<center><button class="btn btn-default" type="submit">Submit</button></center></form>';

					console.log(source);

					$('#order-item').html(opt);


				}
				else
				{
					console.log('No data Found');
				}
			});
		})

		$('.menu-item-decrease').click(function(){

			var param = $(this).attr('data-id');

			var source = 'keranjang/decrease?id=' + param;

			$.get(source, function(data) {

				if (data.length > 0) {

					var opt = '';

					var sub_total = 0;

					var total = 0;

					$.each(data, function(i, el)  {

						opt += '<div class="item">'+ (i+1) +'. '+el.name+ '<br/><small> jumlah: <div class="pull-right">'+ el.jumlah + '</div><br/>harga satuan: <div class="pull-right">Rp' + el.harga + '</div></small></div>';

						sub_total += (el.harga*el.jumlah);

					});

					total = sub_total + (sub_total / 10);

					opt += '<div class="item">Sub Total<div class="pull-right"><small>Rp</small>' + sub_total + '</div></div>';

					opt += '<div class="item"><small>Ppn<div class="pull-right">10%</div></small></div>';

					opt += '<div class="item"><b>Total</b><div class="pull-right"><small>Rp</small><b>' + total + '</b></div></div>';

					opt += '<form action="keranjang/submitpesanan" method="POST" class="form-inline"> {{ csrf_field() }} <center><button class="btn btn-default" type="submit">Submit</button></center></form>';

					console.log(source);

					$('#order-item').html(opt);


				}
				else
				{
					console.log('No data Found');
				}
			});
		})
			
	});
</script>
</html>