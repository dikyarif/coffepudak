@extends('adminlte::page')

@section('title', $meta_title)

@section('content_header')
    <h1>{{$meta_title}}</h1>
@stop

@section('content')

  @if (session('message'))
    <div class="alert alert-info alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{session('message')}}
    </div>
  @endif

  <div class="row">

    <div class="col-md-12">

      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">{{$meta_title}} List</h3>


        </div><!-- end box-header -->

        <div class="box-body table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>Nama Pelayan</th>
                <th>Subtotal</th>
                <th>Total</th>
                <th>Tanggal Transaksi</th>
              </tr>
            </thead>

            <tbody>
              @if(count($datas) > 0)
                @foreach($datas as $data)
                  <tr>
                    <td>{{ $data->user->name }}</td>
                    <td>Rp {{ number_format($data->sub_total,2,',','.') }}</td>
                    <td>Rp {{ number_format($data->total,2,',','.') }}</td>
                    <td>{{ $data->created_at }}</td>
                  </tr>
                @endforeach
              @else
                <tr><td colspan="7">No result found.</td></tr>
              @endif
            </tbody>
           
          </table>
        
        </div><!-- end box-body -->

        <div class="box-footer clearfix">
          @if(count($datas) > 0)
            <div class="pull-left">
              <p><strong>Showing {{ $datas->firstItem() }} to {{ $datas->lastItem() }} of {{ $datas->total() }} entries</strong></p>
            </div>
            <div class="pull-right">
              {{ $datas->links() }}
            </div>
          @endif
        </div><!-- end box-footer -->
        

      </div><!-- end box -->

    </div><!-- end col-md-12 -->

  </div><!-- end row -->

@stop