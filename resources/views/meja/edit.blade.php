@extends('adminlte::page')

@section('title', $meta_title)

@section('content_header')
    <h1>{{$meta_title}}</h1>
@stop

@section('content')

  @if (session('message'))
    <div class="alert alert-info alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{session('message')}}
    </div>
  @endif

  <div class="box">

    <form method="POST" class="form-horizontal" action="{{ url('meja/'.$meja->id.'/edit') }}" enctype="multipart/form-data">
      {{csrf_field()}}

      <div class="box-body">

        <div class="form-group has-feedback{{ $errors->has('name') ? ' has-error' : '' }}">
          <label for="name" class="col-sm-2 control-label">Name *</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" value="{{ $meja->name }}" id="name" name="name" placeholder="Name" required autofocus>
            @if ($errors->has('name'))
                <span class="help-block error">
                    <p>{{ $errors->first('name') }}</p>
                </span>
            @endif
          </div>
        </div><!-- end form-group -->

        </div><!-- end form-group -->

        



      </div><!-- end box-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Save</button>
        <a href="{{url('users')}}" class="btn btn-warning">Back</a>
      </div><!-- end box-footer -->
    </form>

  </div><!-- end box -->

@stop