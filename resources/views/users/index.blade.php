@extends('adminlte::page')

@section('title', $meta_title)

@section('content_header')
    <h1>{{$meta_title}}</h1>
@stop

@section('content')

  @if (session('message'))
    <div class="alert alert-info alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{session('message')}}
    </div>
  @endif

  <div class="row">

    <div class="col-md-12">

      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">{{$meta_title}} List</h3>

          <div class="filter-form">
            <form method="get">
              <input type="text" name="s" placeholder="Search by name, email" class="filter-input" value="{{$keyword}}">

              <div class="filter-select">
                <select name="roles">
                  <option value="" selected>Choose Role</option>
                  @foreach($roles as $role)
                    <option value="{{$role->name}}" {{$role_search == $role->name ? 'selected' : ''}}>{{$role->display_name}}</option>
                  @endforeach
                </select>
              </div><!-- end filter-select -->

              <div class="filter-select">
                <select name="status">
                  <option value="all" selected>Choose Status</option>
                  @php
                    $statusess = Config('constant.status');
                  @endphp

                  @foreach($statusess as $key)
                    <option value="{{$key}}" {{$key == $status ? 'selected' : ''}}>{{ucfirst($key)}}</option>
                  @endforeach
                </select>
              </div><!-- end filter-select -->

              <button type="submit" class="btn btn-primary filter-btn">Search</button>
              <a href="{{url('users')}}" class="btn btn-warning filter-btn">Reset</a>

            </form>

          </div><!-- end filter-form -->

        </div><!-- end box-header -->

        <div class="box-body table-responsive">
          <div class="pull-right">
            <a href="{{ url('users/create') }}" class="btn btn btn-info">Add New</a>
          </div>

          <table class="table table-hover">
            <thead>
              <tr>
                <th>ID</th>
                <th>Avatar</th>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              @if(count($datas) > 0)
                @foreach($datas as $data)
                  <tr>
                    <td>{{ $data->id }}</td>
                    <td>
                      @if($data->avatar)
                        <img src="{{ Storage::disk(Config('constant.storage_disk'))->url(Config('constant.user_avatar_path').$data->avatar) }}" width="45" class="img-circle" alt="{{ $data->name }}">
                      @else
                        <img src="{{ asset('images/avatar.jpg') }}" width="45" class="img-circle" alt="{{ $data->name }}">
                      @endif

                    </td>
                    <td>{{ $data->name }}</td>
                    <td>{{ $data->email }}</td>
                    <td>{{ $data->roles()->get()->first()->display_name }}</td>
                    <td>{{ $data->status == 'active' ? 'Active' : 'Inactive' }}</td>
                    <td>
                    <a href="{{url('users/'.$data->id.'/delete')}}" class="btn btn-danger" title="delete"><i class="fa fa-trash"></i></a> 
                      <a href="{{url('users/'.$data->id.'/edit')}}" class="btn btn-primary" title="Edit"><i class="fa fa-pencil"></i></a>
                    </td>
                  </tr>
                @endforeach
              @else
                <tr><td colspan="7">No result found.</td></tr>
              @endif
            </tbody>
          </table>

        </div><!-- end box-body -->

        <div class="box-footer clearfix">
          @if(count($datas) > 0)
            <div class="pull-left">
    					<p><strong>Showing {{ $datas->firstItem() }} to {{ $datas->lastItem() }} of {{ $datas->total() }} entries</strong></p>
    				</div>
    				<div class="pull-right">
    					{{ $datas->appends(['s'=>$keyword, 'status' => $status, 'roles' => $roles])->links() }}
    				</div>
          @endif
        </div><!-- end box-footer -->

      </div><!-- end box -->

    </div><!-- end col-md-12 -->

  </div><!-- end row -->

@stop