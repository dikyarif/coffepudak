@extends('adminlte::page')

@section('title', $meta_title)

@section('content_header')
    <h1>{{$meta_title}}</h1>
@stop

@section('content')
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
  @if (session('message'))
    <div class="alert alert-info alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{session('message')}}
    </div>
  @endif

  <div class="row">

    <div class="col-md-12">

      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data {{$meta_title}}</h3>


        </div><!-- end box-header -->

        <div class="box-body table-responsive">
          <div class="form-group">
            <label for="">Periode</label>
            <select id="month" class="form-control">
               <option value="">Pilih Bulan--</option>
              <option value="01">January</option>
              <option value="02">February</option>
              <option value="03">March</option>
              <option value="04">April</option>
              <option value="05">May</option>
              <option value="06">June</option>
              <option value="07">July</option>
              <option value="08">August</option>
              <option value="09">September</option>
              <option value="10">October</option>
              <option value="11">November</option>
              <option value="12">December</option>
            </select>
          </div>
          <script>
            $(document).ready(function(){
               $('#month').on('change', function () {
                  var value = $(this).val();
                  var url = "{{url('laporan?month=')}}" + value;  // get selected value
                 
                  window.location = url; // redirect
                 
              });
             });
             
          </script>
          <table class="table table-hover">
            <thead>
              <tr>

                <th>Tanggal Transaksi</th>
                <th>Total Modal</th>
                <th>Uang Masuk</th>
              </tr>
            </thead>
        @php $total = 0; $modal = 0; @endphp
            <tbody>
              @if(count($datas) > 0)
                @foreach($datas as $data)
                  <tr>
                    <td>{{ $data->created_at }}</td>
                    <td>Rp {{ number_format($data->total_modal,2,',','.') }}</td>
                    <td>Rp {{ number_format($data->total,2,',','.') }}</td>
                  </tr>
                  @php $total = $total + $data->total; @endphp
                  @php $modal = $modal + $data->total_modal; @endphp
                @endforeach
                <tr>
                  <td>
                    <td></td>
                    <td></td>
                  </td>
                </tr>
                <tr>
                <td></td>
                  <td>
                    <b>Total uang masuk :</b>
                  </td>
                  <td>
                    Rp {{ number_format($total,2,',','.') }}
                  </td>
                </tr>
                <tr>
                <td></td>
                  <td>
                    <b>Perhitungan Modal :</b>
                  </td>
                  <td>
                    Rp {{ number_format($modal,2,',','.') }}
                  </td>
                </tr>
                 <tr>
                <td></td>
                  <td>
                    <b>Pendapatan bersih :</b>
                  </td>
                  <td>
                  <b>Rp {{ number_format($total-$modal,2,',','.') }}</b>  
                  </td>
                </tr>
              @else
                <tr><td colspan="7">No result found.</td></tr>
              @endif
            </tbody>
           
          </table>
        
        </div><!-- end box-body -->

        <div class="box-footer clearfix">
          @if(count($datas) > 0)
            <div class="pull-left">
              <p><strong>Showing {{ $datas->firstItem() }} to {{ $datas->lastItem() }} of {{ $datas->total() }} entries</strong></p>
            </div>
            <div class="pull-right">
              {{ $datas->links() }}
            </div>
          @endif
        </div><!-- end box-footer -->
        

      </div><!-- end box -->

    </div><!-- end col-md-12 -->

  </div><!-- end row -->

@stop