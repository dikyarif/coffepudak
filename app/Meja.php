<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meja extends Model
{
    protected $table = 'meja';

    public $timestamps = false;

    public function keranjang()
	{
	    return $this->belongsTo('App\Keranjang', 'keranjang_id');
	}
	public function pesanan()
	{
	    return $this->belongsTo('App\Pesanan');
	}
}
