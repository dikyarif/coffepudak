<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{


    public function index()
    {
        if(! auth()->user()->can('view_dashboard')){ abort(404);}

        $data['meta_title'] = 'Home';
        return view('home', $data);
    }
}
