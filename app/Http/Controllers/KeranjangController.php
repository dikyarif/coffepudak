<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Keranjang;
use App\Transaksi;
use App\BarangTransaksi;
use App\Pesanan;
use App\Laporan;
use App\Meja;

use Illuminate\Support\Facades\DB;
class KeranjangController extends Controller
{
    //
    public function index()
    {

    }
    public function store(Request $request)
    {
    	
    	if ( $request->ajax() ) {
    		$keranjang = Keranjang::where('menu_id',$request->id)->first();

    		if(! $keranjang)
    		{
    			$keranjang = new Keranjang;
    			$keranjang->menu_id = $request->id;
    			$keranjang->jumlah = 1;
                $keranjang->meja_id = $request->table;
    			if($keranjang->save())
    			{
    				$keranjang = Keranjang::all();
    				$datas = [];
    				foreach ($keranjang as $data) {
    					array_push($datas,['name' => $data->menu->name, 'jumlah' => $data->jumlah, 'harga' => $data->menu->harga]);
    				}
    				$total = $this->count_total();
    				return $datas;
    			}
    		}
    		else{
    			$keranjang->jumlah = $keranjang->jumlah + 1;
    			if($keranjang->save())
    			{
    				$keranjang = Keranjang::all();
    				$datas = [];
    				foreach ($keranjang as $data) {
    					array_push($datas,['name' => $data->menu->name, 'jumlah' => $data->jumlah, 'harga' => $data->menu->harga]);
    				}
    				$total = $this->count_total();
    				return $datas;
    			}
    		}
        }
        return abort(404);
    }
    public function decrease(Request $request)
    {
    	if ( $request->ajax() ) {
    		$keranjang = Keranjang::where('menu_id',$request->id)->first();

    		if(! $keranjang)
    		{
    			$data = ['error' => 'error', 'message' => 'Barang tersebut sudah kosong di list order'];
    			return $data;
    		}
    		else{
    			if($keranjang->jumlah == 0)
    			{
    				DB::table('keranjang')->where('menu_id', '=', $request->id)->delete();

    				$keranjang = Keranjang::all();

    				$datas = [];

    				foreach ($keranjang as $data) {
    					array_push($datas,['name' => $data->menu->name, 'jumlah' => $data->jumlah, 'harga' => $data->menu->harga]);
    				}

    				$total = $this->count_total();

    				return $datas;
    			}else
    			{
    				$keranjang->jumlah = $keranjang->jumlah - 1;

    				if($keranjang->save())
	    			{
	    				$keranjang = Keranjang::all();

	    				$datas = [];

	    				foreach ($keranjang as $data) {
	    					array_push($datas,['name' => $data->menu->name, 'jumlah' => $data->jumlah, 'harga' => $data->menu->harga]);
	    				}
	    				$total = $this->count_total();
	    				return $datas;
	    			}
    			}
	    			
    		}
        }
        return abort(404);
    }
    public function order(Request $request, $meja_id)
    {

    	$uang = $request->uang;
    	$keranjangs = Pesanan::where('meja_id', $meja_id)->get();
    	$total = 0;
    	$subtotal = 0;
    	foreach ($keranjangs as $keranjang) {
    		$subtotal += ($keranjang->menu->harga*$keranjang->jumlah);
    	}
    	$total = $subtotal + ($subtotal/10);

    	if($uang < $total)
    	{
    		return redirect('/cashier')->with('message', 'Jumlah uang yang dimasukkan tidak cukup');
    	}
    	$transaksi = new Transaksi;
    	$transaksi->id_user = auth()->id();
    	$transaksi->sub_total = $subtotal;
    	$transaksi->total = $total;
    	$kembali = $uang - $total;
    	if($transaksi->save())
    	{
            $laporan = new Laporan;
            $laporan->transaksi_id = $transaksi->id;
            $laporan->total = $transaksi->total;
            $modal = 0;
    		foreach ($keranjangs as $keranjang) {
    			$barang_transaksi = new BarangTransaksi;
    			$barang_transaksi->transaksi_id = $transaksi->id;
    			$barang_transaksi->menu_id = $keranjang->menu_id; 
    			$barang_transaksi->save();
                $modal = $modal + $keranjang->menu->modal;
    		}
            $laporan->total_modal = $modal;
            $laporan->save();
    		Pesanan::where('meja_id', $meja_id)->delete();

    		return redirect('/cashier')->with('message', 'Terima kasih, transaksi telah berhasil dilakukan. Jumlah Kembalian: Rp'.$kembali);
    	}



    }
    public function delete($id){
        $keranjang = Pesanan::where('meja_id', $id);
        $keranjang->delete();
        return redirect('/cashier')->with('message', 'berhasil menghapus pesanan');
    }
    public function storePesanan()
    {
        $keranjangs = Keranjang::all();

        foreach ($keranjangs as $keranjang) {
            $pesanan = new Pesanan;
            $pesanan->menu_id = $keranjang->menu_id;            
            $pesanan->jumlah = $keranjang->jumlah;
            $pesanan->meja_id = $keranjang->meja_id;
            $pesanan->save();
        }
        Keranjang::truncate();
        return redirect('/cashier')->with('message', 'Berhasil input pesanan');
    }
    public function showPesanan(Request $request)
    {
        if ( !$request->ajax() ) { abort(404); }

        $keranjang = Pesanan::where('meja_id', $request->table_id)->get();
        $datas = [];
        foreach ($keranjang as $data) {
            array_push($datas,['name' => $data->menu->name, 'jumlah' => $data->jumlah, 'harga' => $data->menu->harga, 'meja_id' => $request->table_id]);
        }
        $total = $this->count_total();
        return $datas;
    }
    public function pilihmeja(Request $request)
    {
        $id = $request->table_id;
        $data = Meja::where('id', '!=', $id)->get();
        return $data;
    }
    public function gantimeja(Request $request)
    {
        $id = $request->table;
        $new = $request->new;
        Pesanan::where('meja_id', $id)->update(['meja_id' => $new]);
        return redirect('/cashier')->with('message', 'Sukses mengganti Meja');
    
    }
    protected function count_total()
    {
    	$keranjangs = Keranjang::all();
    	$total = 0;
    	foreach ($keranjangs as $keranjang) {
    		$total += ($keranjang->jumlah * $keranjang->menu->harga);
    	}
    	return $total;
    }
}
