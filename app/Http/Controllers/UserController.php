<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use Storage;

class UserController extends Controller
{
    private $filter_roles;

    public function __construct(){
      $this->filter_roles = Role::orderBy('display_name', 'ASC')->get();
    }

    public function index(Request $request)
    {
      if(! auth()->user()->can('view_users')) { abort(404);}

      $data['meta_title'] = 'Users';

      $data['keyword'] = $keyword = $request->s;
      $data['role_search'] = $role_search = $request->roles;
      $data['status'] = $status = $request->status;

      $data['datas'] = User::users($keyword, $role_search, $status)
        ->orderBy('users.id', 'DESC')
        ->paginate(Config('constant.view_per_page'));
      $data['roles'] = $this->filter_roles;

      return view('users.index', $data);
    }


    public function create()
    {
        if(! auth()->user()->can('create_user')) { abort(404); }

        $data['meta_title'] = 'Create User';
        $data['roles'] = $this->filter_roles;

        return view('users.add', $data);
    }


    public function store(Request $request)
    {
        if(! auth()->user()->can('create_user')) { abort(404); }
        $rules = [
          'name' => 'required',
          'email' => 'required|unique:users,email',
          'username' => 'unique:users,username',
          'password' => 'required|min:6',
          'role' => 'required',
        ];
        if($request->file('avatar')){
          $rules['avatar'] = 'mimes:jpeg,gif,png|max:1024';
        }

        $this->validate($request, $rules);

        $get_username = explode('@', $request->email);



        $result = new User;
        $result->name = $request->name;
        $result->email = $request->email;
        $result->password = bcrypt($request->password);
        $result->username = $get_username[0];
        $result->status = $request->status;

        if($request->file('avatar')){
          $avatar = $request->file('avatar');
          $avatar_name = pathinfo($avatar->getClientOriginalName(), PATHINFO_FILENAME);
          $avatar_ext = $avatar->getClientOriginalExtension();
          $hash_microtime = substr(md5(microtime()), 15, 5);
          $avatar_filename = str_slug($avatar_name)."_".$hash_microtime.".".$avatar_ext;
          $result->avatar = $avatar_filename;

          $path = Config('constant.user_avatar_path');
          $storage_disk = Config('constant.storage_disk');
          $save_avatar = Storage::disk($storage_disk)->putFileAs($path, $avatar, $avatar_filename, 'public');

        }

        if($result->save()){
            $result->attachRole($request->role);

            return redirect('/users')->with('message', 'Success add'.' '.$result->name);
        }

        return back();

    }


    public function show($id)
    {
        abort(404);
    }


    public function edit($id)
    {
      if(! auth()->user()->can('edit_user')) { abort(404); }

      $user = User::findOrFail($id);

      $data['meta_title'] = 'Edit User '.$user->name;
      $data['roles'] = $this->filter_roles;
      $data['data'] = $user;

      return view('users.edit', $data);
    }


    public function update(Request $request, $id)
    {
        if(! auth()->user()->can('edit_user')) { abort(404); }

        $password = $request->password;
        $result = User::findOrFail($id);

        $rules = array(
          'name' => 'required',
          'role' => 'required',
        );
        if($password) {
            $rules['password'] = 'required|min:6';
        }

        if($request->file('avatar')){
          $rules['avatar'] = 'mimes:jpeg,gif,png|max:1024';
        }

        $result->name = $request->name;

        if($password)
          $result->password = bcrypt($password);

        $result->status = $request->status;
        $old_avatar_name = $result->avatar;
        if($request->file('avatar')){
          $avatar = $request->file('avatar');
          $avatar_name = pathinfo($avatar->getClientOriginalName(), PATHINFO_FILENAME);
          $avatar_ext = $avatar->getClientOriginalExtension();
          $hash_microtime = substr(md5(microtime()), 15, 5);
          $avatar_filename = str_slug($avatar_name)."_".$hash_microtime.".".$avatar_ext;
          $result->avatar = $avatar_filename;

          $path = Config('constant.user_avatar_path');
          $storage_disk = Config('constant.storage_disk');
          $save_avatar = Storage::disk($storage_disk)->putFileAs($path, $avatar, $avatar_filename, 'public');

        }

        if($result->save()){
            // we need to remove old avatar if they are upload new one
            if($request->file('avatar')){
              if($old_avatar_name){
                  Storage::disk(Config('constant.storage_disk'))->delete(Config('constant.user_avatar_path').$old_avatar_name);
              }

            }

            // We need to detach and attach the role
            $old_role = $result->roles()->get()->first()->id;
            $result->detachRole($old_role);
            $result->attachRole($request->role);
            return redirect('/users')->with('message', 'Success edit'.' '.$result->name);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
    }
    public function delete($id){
       if(! auth()->user()->can('edit_user')) { abort(404); }
      User::destroy($id);
      return redirect('users');

    }
    
}
