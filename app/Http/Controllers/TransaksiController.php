<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaksi;
class TransaksiController extends Controller
{
    public function index()
    {
      if(! auth()->user()->can('Transaksi')) { abort(404);}

      $data['meta_title'] = 'Transaksi';


      $data['datas'] = Transaksi::paginate(12);

      return view('transaksi.index', $data);
    }
}
