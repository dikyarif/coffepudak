<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Laporan;
class LaporanController extends Controller
{
    public function index(Request $request)
    {
      if(! auth()->user()->can('view_laporan')) { abort(404);}

      if($request->has('month'))
      {
      	$data['datas'] = Laporan::whereMonth('created_at', '=', $request->month)->paginate(12);
      	$data['bulan'] = $request->month;
      }
      else
      {
      	$data['datas'] = Laporan::paginate(12);
      }
      $data['meta_title'] = 'Laporan';


      

      return view('laporan.index', $data);
    }
}
