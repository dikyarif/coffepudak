<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AdminMenu;
use App\Models\Permission;
use App\Models\Role;

class SettingController extends Controller
{
    private $menus_parents, $menus_permissions;

    public function __construct(){

      $this->menus_parents = AdminMenu::where('parent', 0)
        ->orderBy('title', 'ASC')
        ->get();

      $this->menus_permissions = Permission::all();

    }

    // MENUS
    public function menus(){

      if(! auth()->user()->can('view_menus')){ abort(404);}

      $data['meta_title'] = 'Menus';

      $data['datas'] = AdminMenu::orderBy('id', 'DESC')
        ->paginate(Config('constant.view_per_page'));

      return view('settings.menus', $data);
    }

    public function addMenu(){

      if(! auth()->user()->can('create_menu')){ abort(404);}

      $data['meta_title'] = 'Add Menu';
      $data['parents'] = $this->menus_parents;
      $data['permissions'] = $this->menus_permissions;

      return view('settings.addMenu', $data);

    }

    public function postAddMenu(Request $request){
      if(! auth()->user()->can('create_menu')){ abort(404);}

      $this->validate($request, [
        'title' => 'required',
        'parent' => 'required',
        'url' => 'required'
      ]);

      $result = new AdminMenu;

      $result->title = $request->title;
      $result->parent = $request->parent;
      $result->url = $request->url;
      $result->icon = $request->icon;
      $result->status = $request->status;
      $result->visibility = $request->visibility;
      $result->permission = $request->permission;
      $result->sort_order = $request->sort_order;

      if($result->save()){
        return redirect('/settings/menus')->with('message', 'Success add '.$result->title.'['.$result->id.']');
      }

      return back();
    }

    public function editMenu($id){
      if(! auth()->user()->can('edit_menu')){ abort(404);}

      $menu = AdminMenu::findOrFail($id);

      $data['meta_title'] = 'Edit Menu '.$menu->title;
      $data['data'] = $menu;
      $data['parents'] = $this->menus_parents;

      $data['permissions'] = $this->menus_permissions;

      return view('settings.editMenu', $data);
    }

    public function postEditMenu(Request $request, $id){
      if(! auth()->user()->can('edit_menu')){ abort(404);}

      $this->validate($request, [
        'title' => 'required',
        'parent' => 'required',
        'url' => 'required'
      ]);


      $result = AdminMenu::findOrFail($id);

      $result->title = $request->title;
      $result->parent = $request->parent;
      $result->url = $request->url;
      $result->icon = $request->icon;
      $result->status = $request->status;
      $result->visibility = $request->visibility;
      $result->permission = $request->permission;
      $result->sort_order = $request->sort_order;

      if($result->save()){
        return redirect('/settings/menus')->with('message', 'Success edit '.$result->title.'['.$result->id.']');
      }

      return back();
    }

    public function deleteMenu($id){
      if(! auth()->user()->hasRole('super_administrator')) {abort(404);}

      $result = AdminMenu::findOrFail($id);
      if($result->delete()){
        return redirect('/settings/menus')->with('message', 'Success delete '.$result->title.'['.$result->id.']');
      }


    }
    // END MENUS

    // ROLES
    public function roles(){
      if(! auth()->user()->can('view_roles')){ abort(404);}

      $data['meta_title'] = 'Roles';

      $data['datas'] = Role::orderBy('id', 'DESC')
        ->paginate(Config('constant.view_per_page'));

      return view('settings.roles', $data);
    }

    public function addRole(){

      if(! auth()->user()->can('create_role')){ abort(404);}

      $data['meta_title'] = 'Add Role';

      return view('settings.addRole', $data);

    }

    public function postAddRole(Request $request){
      if(! auth()->user()->can('create_role')){ abort(404);}

      $this->validate($request, [
        'name' => 'required|unique:roles,name',
        'display_name' => 'required',
        'description' => 'max:255'
      ]);

      $result = new Role;

      $result->name = $request->name;
      $result->display_name = $request->display_name;
      $result->description = $request->description;

      if($result->save()){
        return redirect('/settings/roles')->with('message', 'Success add '.$result->display_name.'['.$result->id.']');
      }

      return back();
    }

    public function editRole($id){
      if(! auth()->user()->can('edit_role')){ abort(404);}

      $result = Role::findOrFail($id);

      $data['meta_title'] = 'Edit Role '.$result->display_name;
      $data['data'] = $result;

      return view('settings.editRole', $data);
    }

    public function postEditRole(Request $request, $id){
      if(! auth()->user()->can('edit_role')){ abort(404);}

      $this->validate($request, [
        // 'name' => 'required|unique:roles,name',
        'display_name' => 'required',
        'description' => 'max:255'
      ]);

      $result = Role::findOrFail($id);

      $result->display_name = $request->display_name;
      $result->description = $request->description;

      if($result->save()){
        return redirect('/settings/roles')->with('message', 'Success edit '.$result->display_name.'['.$result->id.']');
      }

      return back();
    }

    public function editRolePermission($id){
      if(! auth()->user()->can('edit_role_permission')){ abort(404);}

      $result = Role::findOrFail($id);

      $data['meta_title'] = 'Edit Role Permission for '.$result->display_name;
      $data['permissions'] = $this->menus_permissions;
      $data['selected_permissions'] = $result->permissions->pluck('id')->toArray();
      $data['id'] = $result->id;

      return view('settings.editRolePermission', $data);
    }

    public function postEditRolePermission(Request $request, $id){
      if(! auth()->user()->can('edit_role_permission')){ abort(404);}

      $result = Role::findOrFail($id);

      $permissions = $request->permissions;

      // dettach permission from role
      $result->detachPermissions($result->permissions);

      if(count($permissions) > 0){
          // attach permission to role
          $result->attachPermissions($permissions);

          return redirect('/settings/roles')->with('message', 'Success edit role permission for '.$result->display_name);
      }

      return redirect('/settings/roles');
    }

    // END ROLES

    // PERMISSIONS
    public function permissions(){
      if(! auth()->user()->can('view_permissions')){ abort(404);}

      $data['meta_title'] = 'Permissions';

      $data['datas'] = Permission::orderBy('id', 'DESC')
        ->paginate(Config('constant.view_per_page'));

      return view('settings.permissions', $data);
    }

    public function addPermission(){
      if(! auth()->user()->can('create_permission')){ abort(404);}

      $data['meta_title'] = 'Add Permission';

      return view('settings.addPermission', $data);

    }

    public function postAddPermission(Request $request){
      if(! auth()->user()->can('create_permission')){ abort(404);}

      $this->validate($request, [
        'name' => 'required|unique:permissions,name',
        'display_name' => 'required',
        'description' => 'max:255'
      ]);

      $result = new Permission;

      $result->name = $request->name;
      $result->display_name = $request->display_name;
      $result->description = $request->description;

      if($result->save()){
        return redirect('/settings/permissions')->with('message', 'Success add '.$result->display_name.'['.$result->id.']');
      }

      return back();
    }

    public function editPermission($id){
      if(! auth()->user()->can('edit_permission')){ abort(404);}

      $result = Permission::findOrFail($id);

      $data['meta_title'] = 'Edit Permission '.$result->display_name;
      $data['data'] = $result;

      return view('settings.editPermission', $data);
    }

    public function postEditPermission(Request $request, $id){
      if(! auth()->user()->can('edit_permission')){ abort(404);}

      $this->validate($request, [
        'display_name' => 'required',
        'description' => 'max:255'
      ]);

      $result = Permission::findOrFail($id);

      $result->display_name = $request->display_name;
      $result->description = $request->description;

      if($result->save()){
        return redirect('/settings/permissions')->with('message', 'Success edit '.$result->display_name.'['.$result->id.']');
      }

      return back();
    }

    // END PERMISSIONS


}
