<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;
use Storage;
class MenuController extends Controller
{
    //
    public function index()
    {
    	if(! auth()->user()->can('view_daftar_menu')) { abort(404);}

    	$data['meta_title'] = 'Menu';
    	$data['datas'] = Menu::paginate(Config('constant.view_per_page'));
    	return view('menu.index', $data);
    }
    public function create()
    {
    	if(! auth()->user()->can('create_menu_makanan')) { abort(404);}

    	$data['meta_title'] = 'Tambah Menu';

    	return view('menu.create', $data);
    }
    public function store(Request $request)
    {
    	if(! auth()->user()->can('create_menu_makanan')) { abort(404); }

        $rules = [
          'name' => 'required|unique:menus,name',
          'jenis' => 'required',
          'harga' => 'required|min:0',
        ];

        if($request->file('image')){
          $rules['image'] = 'mimes:jpeg,gif,png|max:1024';
        }

        $this->validate($request, $rules);

        



        $result = new Menu;
        $result->name = $request->name;
        $result->jenis = $request->jenis;
        $result->modal = $request->modal;
        $result->harga = $request->harga;
        $result->status = $request->status;

        if($request->file('image')){
          $image = $request->file('image');
          $image_name = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
          $image_ext = $image->getClientOriginalExtension();
          $hash_microtime = substr(md5(microtime()), 15, 5);
          $image_filename = str_slug($image_name)."_".$hash_microtime.".".$image_ext;
          $result->image = $image_filename;

          $path = Config('constant.menu_image_path');
          $storage_disk = 'public';
          $save_image = Storage::disk($storage_disk)->putFileAs($path, $image, $image_filename, 'public');

        }

        if($result->save()){

            return redirect('/daftar-menu')->with('message', 'Success add'.' '.$result->name);
        }else{
        	
            dd($result);
        }

            dd($result);
    }
    public function edit($id)
    {
    	if(! auth()->user()->can('create_menu_makanan')) { abort(404); }

      $menu = Menu::findOrFail($id);

      $data['meta_title'] = 'Edit Menu '.$menu->name;

      $data['data'] = $menu;

      return view('menu.edit', $data);
    }
    public function update(Request $request, $id)
    {
        if(! auth()->user()->can('create_menu_makanan')) { abort(404); }

        $result = Menu::findOrFail($id);

        $rules = array(
          'name' => 'required'
        );

        if($request->file('image')){
          $rules['image'] = 'mimes:jpeg,gif,png|max:1024';
        }

        $result->name = $request->name;
        $result->harga = $request->harga;
        $result->jenis = $request->jenis;
        $result->modal = $request->modal;
        $result->status = $request->status;

        $old_image_name = $result->image;

        if($request->file('image')){

          $image = $request->file('image');
          $image_name = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
          $image_ext = $image->getClientOriginalExtension();
          $hash_microtime = substr(md5(microtime()), 15, 5);
          $image_filename = str_slug($image_name)."_".$hash_microtime.".".$image_ext;
          $result->image = $image_filename;

          $path = Config('constant.menu_image_path');
          $storage_disk ='public';
          $save_image = Storage::disk($storage_disk)->putFileAs($path, $image, $image_filename, 'public');

        }

        if($result->save()){
            // we need to remove old image if they are upload new one
            if($request->file('image')){
              if($old_image_name){
                  Storage::disk('public')->delete(Config('constant.menu_image_path').$old_image_name);
              }

            }

            // We need to detach and attach the role
            return redirect('/daftar-menu')->with('message', 'Success edit'.' '.$result->name);
        }
    }
}
