<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;

class ProfileController extends Controller
{
    public function index(){
      $current_user = auth()->user();
      $data['meta_title'] = 'Edit Profile '.$current_user->name;
      $data['data'] = $current_user;
      return view('profile.index', $data);
    }

    public function post(Request $request){

      $result = auth()->user();

      $rules = array(
        'name' => 'required',
      );
      $password = $request->password;

      if($password) {
          $rules['password'] = 'required|min:6';
      }

      if($request->file('avatar')){
        $rules['avatar'] = 'mimes:jpeg,gif,png|max:1024';
      }

      $result->name = $request->name;

      if($password)
        $result->password = bcrypt($password);

      $old_avatar_name = $result->avatar;
      if($request->file('avatar')){
        $avatar = $request->file('avatar');
        $avatar_name = pathinfo($avatar->getClientOriginalName(), PATHINFO_FILENAME);
        $avatar_ext = $avatar->getClientOriginalExtension();
        $hash_microtime = substr(md5(microtime()), 15, 5);
        $avatar_filename = str_slug($avatar_name)."_".$hash_microtime.".".$avatar_ext;
        $result->avatar = $avatar_filename;

        $path = Config('constant.user_avatar_path');
        $storage_disk = Config('constant.storage_disk');
        $save_avatar = Storage::disk($storage_disk)->putFileAs($path, $avatar, $avatar_filename, 'public');

      }

      if($result->save()){
          // we need to remove old avatar if they are upload new one
          if($request->file('avatar')){
            if($old_avatar_name){
                Storage::disk(Config('constant.storage_disk'))->delete(Config('constant.user_avatar_path').$old_avatar_name);
            }

          }

          return redirect('/profile')->with('message', 'Success edit profile'.' '.$result->name);
      }
    }
}
