<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;
use App\Keranjang;
use App\Meja;
use App\Pesanan;
class CashierController extends Controller
{
    public function index()
    {
    	$data['meta_title'] = 'Cashier';

    	$data['menus'] = Menu::where('status',1)->get();
    	$data['tables'] = Meja::all();
    	$data['pesanan'] = Pesanan::all();
    	$data['table_pesanan'] = Pesanan::select('meja_id','created_at')->groupBy('meja_id','created_at')->get();
    	$data['keranjangs'] = Keranjang::all();

    	return view('cashier.index', $data);
    }
}
