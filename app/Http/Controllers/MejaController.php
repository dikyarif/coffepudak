<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meja;
class MejaController extends Controller
{
    public function index(){
    	if(! auth()->user()->can('tambah_meja')) { abort(404);}

    	$data['meta_title'] = 'Meja';


      	$data['datas'] = Meja::paginate(12);

      	return view('meja.index', $data);
    }
    public function createform(){
    	if(! auth()->user()->can('tambah_meja')) { abort(404);}
    	$data['meta_title'] = 'Add Meja';
    	return view('meja.add', $data);
    }

    public function create(Request $request){
    	if(! auth()->user()->can('tambah_meja')) { abort(404);}
    	$meja = new Meja;
    	$meja->name = $request->name;

    	$meja->save();

    	return redirect('meja');

    }
    public function delete($id){
    	if(! auth()->user()->can('tambah_meja')) { abort(404);}
    	Meja::destroy($id);
    	return redirect('meja');
    }
    public function editform($id){
    	if(! auth()->user()->can('tambah_meja')) { abort(404);}
    	$data['meja'] = Meja::find($id);
    	$data['meta_title'] = 'Edit Meja';
    	return view('meja.edit', $data);
    }
    public function update(Request $request, $id){
    	if(! auth()->user()->can('tambah_meja')) { abort(404);}
    	$meja = Meja::find($id);
    	$meja->name = $request->name;

    	$meja->save();

    	return redirect('meja');

    }
}
