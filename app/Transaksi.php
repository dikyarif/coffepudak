<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = 'transaksi';

    public function user()
	{
	    return $this->belongsTo('App\Models\User', 'id_user');
	}
	public function laporan()
	{
	    return $this->hasOne('App\Laporan');
	}
}
