<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarangTransaksi extends Model
{
    protected $table = 'barang_transaksi';

    public function transaksi()
	{
	    return $this->belongsTo('App\Transaksi', 'transaksi_id');
	}
	public function menu()
	{
	    return $this->belongsTo('App\Models\Menus', 'menu_id');
	}
}
