<?php

namespace App\Filters;

use JeroenNoten\LaravelAdminLte\Menu\Builder;
use JeroenNoten\LaravelAdminLte\Menu\Filters\FilterInterface;
use Auth;

class MenuFilter implements FilterInterface
{


    public function transform($item, Builder $builder)
    {
        if (isset($item['permission']) && ! Auth::user()->can($item['permission'])) {
            return false;
        }

        return $item;
    }
}
