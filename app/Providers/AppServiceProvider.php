<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;
use Illuminate\Contracts\Events\Dispatcher;
use App\Models\AdminMenu;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
          $event->menu->add('MAIN NAVIGATION');

          // find parent menus
          $admin_menus_parent = AdminMenu::where('parent',0)
            ->where('status', 1)
            ->where('visibility', 1)
            ->orderBy('sort_order', 'ASC')
            ->get();
          foreach ($admin_menus_parent as $amp) {
              $m['text'] = $amp->title;
              $m['url'] = $amp->url;
              $m['icon'] = $amp->icon;
              $m['permission'] = $amp->permission;

              // find sub menus
              $submenus = AdminMenu::where('parent', $amp->id)
                ->where('status', 1)
                ->where('visibility', 1)
                ->orderBy('sort_order', 'ASC')
                ->get();
              if(count($submenus) > 0){
                  $sub_array = array();

                  foreach($submenus as $sm){
                      $sub['text'] = $sm->title;
                      $sub['url'] = $sm->url;
                      $sub['permission'] = $sm->permission;

                      array_push($sub_array, $sub);
                  }

                  $m['submenu'] = $sub_array;
              }

              $event->menu->add($m);
          }

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
