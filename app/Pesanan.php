<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pesanan extends Model
{
    protected $table = 'pesanan';

    public function menu()
	{
	    return $this->belongsTo('App\Models\Menu', 'menu_id');
	}
	public function mejas()
	{
	    return $this->hasOne('App\Meja', 'id', 'meja_id');
	}
}
