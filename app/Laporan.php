<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Laporan extends Model
{
    //
    protected $table = 'laporan';

    public function transaksi()
	{
	    return $this->belongsTo('App\Transaksi');
	}
}
