<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menus';

    public function keranjang()
    {
        return $this->hasMany('App\Keranjang', 'keranjang_id');
    }
    public function pesanan()
    {
        return $this->hasMany('App\Pesanan', 'pesanan_id');
    }
}
