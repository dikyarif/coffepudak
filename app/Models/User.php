<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use App\Models\Role;

class User extends Authenticatable
{
    use Notifiable, LaratrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $status_text = [
        0 => 'inactive',
        1 => 'active'
    ];

    public function getStatusAttribute($value){
      return $this->status_text[$value];
    }
    public function transaksi()
    {
        return $this->hasMany('App\Transaksi', 'transaksi_id');
    }

    /* Get All users with filter */
    public function scopeUsers($query, $keyword, $roles, $status){
        // filter by role
        if($roles){
            $q = Role::where('roles.name', '=', $roles)->first();
            $users = $q->users()->where(function($q_user) use($keyword, $status){
                $this->filter_users($q_user, $keyword, $status);
            });
        }else{
            $users = $this->filter_users($query, $keyword, $status);
        }
        return $users;
    }

    /* Where clause for users */
    protected function filter_users($query, $keyword, $status){
        $q = $query->where(function($q_keyword) use($keyword){
            $q_keyword->where('users.name', 'like', '%'.$keyword.'%')->orWhere('users.email', 'like', '%'.$keyword.'%');
        });

        if($status == 'all' || $status == ''){
            return $q;
        }
        else{

            $allowed_status = array_flip($this->status_text);
            if (array_key_exists($status, $allowed_status)) {
                return $query->where('status', $allowed_status[$status]);
            }
        }
    }

    
}
