<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keranjang extends Model
{
    protected $table = 'keranjang';

    public function menu()
	{
	    return $this->belongsTo('App\Models\Menu', 'menu_id');
	}
	public function meja()
	{
	    return $this->has('App\Meja');
	}
}
