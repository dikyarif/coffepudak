# Quantum Engine

Simple back-ends / administrator page, Build from Laravel 5.5.

### What inside?
1. [AdminLTE](https://github.com/jeroennoten/Laravel-AdminLTE)
2. [Roles And Permissions](https://github.com/santigarcor/laratrust)
3. User Management with avatar
4. Dynamic Menus with permission

### Installation
To installation just clone / download this repository and run
```
composer install
```

```
php artisan migrate
```

```
php artisan db:seed
```

### Upload Avatar Configuration
Default 'storage_disk' = 'public'
You can change the 'storage_disk' from :
```html
app/config/constant.php
```
If You're using s3 , You need to setup AWS Configuration in .env
```
AWS_KEY=
AWS_SECRET=
AWS_REGION=
AWS_BUCKET=
```