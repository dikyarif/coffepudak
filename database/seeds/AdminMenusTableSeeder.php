<?php

use Illuminate\Database\Seeder;
use App\Models\AdminMenu;

class AdminMenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      AdminMenu::truncate();

      AdminMenu::create([
        'title' => 'Dashboard',
        'parent' => 0,
        'url' => '/',
        'icon' => 'home',
        'permission' => 'view_dashboard',
        'sort_order' => 1
      ]);

      AdminMenu::create([
        'title' => 'Users',
        'parent' => 0,
        'url' => '/users',
        'icon' => 'users',
        'permission' => 'view_users',
        'sort_order' => 2
      ]);

      AdminMenu::create([
        'title' => 'Settings',
        'parent' => 0,
        'url' => '#',
        'icon' => 'cog',
        'permission' => 'view_settings',
        'sort_order' => 3
      ]);

      AdminMenu::create([
        'title' => 'Menus',
        'parent' => 3,
        'url' => '/settings/menus',
        'permission' => 'view_menus',
        'sort_order' => 1
      ]);

      AdminMenu::create([
        'title' => 'Roles',
        'parent' => 3,
        'url' => '/settings/roles',
        'permission' => 'view_roles',
        'sort_order' => 2
      ]);

      AdminMenu::create([
        'title' => 'Permissions',
        'parent' => 3,
        'url' => '/settings/permissions',
        'permission' => 'view_permissions',
        'sort_order' => 3
      ]);

    }
}
