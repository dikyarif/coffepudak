<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // Permission::truncate();

      Permission::create([
        'name' => 'view_dashboard',
        'display_name' => 'View Dashboard'
      ]);

      Permission::create([
        'name' => 'view_users',
        'display_name' => 'View Users'
      ]);

      Permission::create([
        'name' => 'create_user',
        'display_name' => 'Create User'
      ]);

      Permission::create([
        'name' => 'edit_user',
        'display_name' => 'Edit User'
      ]);

      Permission::create([
        'name' => 'view_settings',
        'display_name' => 'View Settings'
      ]);

      Permission::create([
        'name' => 'view_menus',
        'display_name' => 'View Menus'
      ]);

      Permission::create([
        'name' => 'create_menu',
        'display_name' => 'Create Menu'
      ]);

      Permission::create([
        'name' => 'edit_menu',
        'display_name' => 'Edit Menu'
      ]);

      Permission::create([
        'name' => 'view_roles',
        'display_name' => 'View Roles'
      ]);

      Permission::create([
        'name' => 'create_role',
        'display_name' => 'Create Role'
      ]);

      Permission::create([
        'name' => 'edit_role',
        'display_name' => 'Edit Role'
      ]);

      Permission::create([
        'name' => 'edit_role_permission',
        'display_name' => 'Edit Role Permission'
      ]);

      Permission::create([
        'name' => 'view_permissions',
        'display_name' => 'View Permissions'
      ]);

      Permission::create([
        'name' => 'create_permission',
        'display_name' => 'Create Permission'
      ]);

      Permission::create([
        'name' => 'edit_permission',
        'display_name' => 'Edit Permission'
      ]);

    }
}
