<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // Role::truncate();

      Role::create([
        'name' => 'super_administrator',
        'display_name' => 'Super Administrator'
      ]);

      Role::create([
        'name' => 'author',
        'display_name' => 'Author'
      ]);

      $role1 = Role::find(1)->attachPermissions([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]);
      $role2 = Role::find(2)->attachPermissions([1]);
    }
}
