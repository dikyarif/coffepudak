<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      User::truncate();

      User::create([
        'name' => 'Alfredo Indra Cahya',
        'email' => 'alfredoindra@gmail.com',
        'username' => 'alfredoic',
        'password' => bcrypt('adminadmin'),
        'status' => 1
      ]);

      User::create([
        'name' => 'Dodo',
        'email' => 'freic1989@gmail.com',
        'username' => 'freic1989',
        'password' => bcrypt('adminadmin'),
        'status' => 1
      ]);

      $user1 = User::find(1)->attachRole(1);
      $user2 = User::find(2)->attachRole(2);
    }
}
